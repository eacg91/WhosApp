# WhosApp

Cliente libre de WhattsApp para escritorio. Se utilizan las librerias yowsup, programadas por tgalal en lenguaje python.
Para ejecutar la aplicación WhosApp (con interfáz gráfica) utilice el archivo src/main.java
Para ejecutar yowsup (por lineas de texto y comando) utilice src/yowsup-cli

# yowsup-cli

Pequeña aplicación de demostración de las librerias Yowsup, incluida en el mismo repositorio GIT de las librerias Yowsup. Está programado también en Python e inicialmente sólo sirve para dar de alta una cuenta en el servidor de WhatsApp, también para enviar y recibir mensajes de texto.
En el actual proyecto se busca ampliar las características de yowsup-cli, incorporando el envío de imagenes, videos y sonidos.

Antes de proceder con la instalación de Yowsup se debe estar seguro de cumplir con las dependencias dateutil y argparse (que son extensiones de python). Las dependencias pueden ser instaladas mediante el comando:

sudo yum install python-pip python-dateutil python-argparse python-imaging python-setuptools python-dev libncurses5-dev libevent-dev

# DEPRECATED (Obsoleto)

El actual protocolo de WhatsApp inscribe a lista negra los números telefónicos que utilicen aplicaciones de terceros.
Tan pronto envíe su primer mensaje con WhosApp (este proyecto), WhatsApp bloqueará indefinidamente su numero para continuar operando.

# Posibilidad para continuar su uso

Teoricamente sería posible hacer un hacking en red local, para "escuchar" la red durante el registro de un numero telefónico en la app de WhatsApp de un telefono móvil.
De hacer esto, se pueden obtener las credenciales de acceso a la cuenta WhatsApp y utilizarlas para probar el funcionamiento de WhosApp (este proyecto).
ESTE PROYECTO NO REALIZA NINGUNA FUNCIÓN DE HACKING.